const select = document.getElementById("fonts");
const textarea = document.getElementById("demo-font");

select.addEventListener("change", function(){
  const seleccion = select.value;
  textarea.setAttribute("class", seleccion);
});